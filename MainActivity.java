package pl.animo2233.locationservice;

import android.Manifest;
import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.MapView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {


    private static final String TAG = "Animo2233";
    private static final int LOCATION_PERM_REQUEST = 21;
    private boolean isServiceStarted = false;
    private MyReceiver myReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        myReceiver = new MyReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(GetLocationService.ACTION_DATA_PASS);
        registerReceiver(myReceiver, intentFilter);

        if(isMyServiceRunning(GetLocationService.class)){
            isServiceStarted = true;

            Button button = (Button) findViewById(R.id.localiseButton);
            button.setText("Localising...");

            Toast.makeText(getApplicationContext(), "Service already running...", Toast.LENGTH_SHORT).show();
        }


    }

    public void localise(View v){
        Log.i(TAG, "OnButtonClick");


        if(checkRequiredPermissions()) {

            Button button = (Button) v;

            //Check for location
            try {
                int checkGPSsignal = Settings.Secure.getInt(getContentResolver(), Settings.Secure.LOCATION_MODE);
                if(checkGPSsignal == 0){
                    Intent onGPS = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(onGPS);
                }
            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
            }


            if (!isServiceStarted) {
                Intent intent = new Intent(this, GetLocationService.class);
                startService(intent);
                isServiceStarted = true;
                button.setText("Localising...");
            } else {
                //Disable service
                Intent intent = new Intent(this, GetLocationService.class);
                stopService(intent);
                Log.i(TAG, "The service should be stopped now.");
                isServiceStarted = false;
                button.setText("Localise Me!");
            }
        }

    }

    public boolean checkRequiredPermissions(){
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){

            if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                    ||  ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)){

                //Trzeba poprosic o permisje
                Log.i(TAG, "TRZEBA POPROSIC O PERMISJE");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_PERM_REQUEST);
            } else {
                Log.i(TAG, "Prosimy Permisje");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_PERM_REQUEST );
            }
        }else{
            Log.i(TAG, "Permisje GRANTED");
            return true;
        }


        return false;

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,  String permissions[], int[] grantResults) {
        switch (requestCode) {
            case LOCATION_PERM_REQUEST: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    localise(findViewById(R.id.localiseButton));
                } else {
                    Toast.makeText(getApplicationContext(), "Musisz wyrazić zgodę na korzystanie z lokalizacji.", Toast.LENGTH_LONG).show();
                    Log.i(TAG, "Toast displayed");
                }
                return;
            }

        }
    }

    private class MyReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent args) {
            // TODO Auto-generated method stub

            Location loc = args.getParcelableExtra("LOCATION");

            Toast.makeText(getApplicationContext(),
                    "Triggered by Service!\n"
                            + "Location: " + loc.getLatitude() +", "+ loc.getLongitude(),
                    Toast.LENGTH_LONG).show();

        }

    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

}
